import { AboutComponent } from './about/about.component';
import { DesignutilityService } from './appServices/designutility.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { CardComponent } from './card/card.component';

import { ClassComponent } from './class/class.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AppListIconDirective, ListItemDirective } from './about/about.directive';


@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    CardComponent,
    ClassComponent,
    AboutComponent,
    AppListIconDirective,
    ListItemDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
  ],
  providers: [DesignutilityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
