import { Component, Inject, OnInit } from '@angular/core';
import { BOARD_DAO_TOKEN } from '../about/about.component';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {

  constructor(@Inject(BOARD_DAO_TOKEN) myString: string) { }
  // tslint:disable-next-line: member-ordering
  // tslint:disable-next-line: no-inferrable-types
  // tslint:disable-next-line: member-ordering
  msg = '';

  // tslint:disable-next-line: member-ordering
  // tslint:disable-next-line: no-inferrable-types
  uname: string = 'alina';

  isValid = false;
  products = [
    {proId : '1', proName : 'Laptop', proPrice : '24'},
    {proId: '2', proName: 'tv', proPrice : '2400'}
  ];

  ngOnInit(): void {


  }
  // tslint:disable-next-line: typedef
  AddMe(event){
    this.msg = event.target.value + ' added in cart';
  }

  // tslint:disable-next-line: typedef
  getInputInfo(inputinfo){
    console.log(inputinfo) ;
  }

  // tslint:disable-next-line: typedef
  onCreate(){
    this.isValid=true ;
  }
}
