import { ContentChild, Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[appListIcon]' })
export class AppListIconDirective {
  constructor(public templateRef: TemplateRef<any>) { }
}

@Directive({ selector: '[appListItem]' })
export class ListItemDirective {
  @ContentChild(AppListIconDirective, { static: false })
  public listIconDirective: AppListIconDirective;

  constructor(public templateRef: TemplateRef<any>) { }
}
