import { AfterContentInit, Component, ContentChild, Inject, InjectionToken } from '@angular/core';
import { AppListIconDirective } from './about.directive';


export const BOARD_DAO_TOKEN = new InjectionToken <string>(
  'service component');

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  providers: [ { provide: BOARD_DAO_TOKEN, useValue: 'This stringsss' } ],
//   template: `
//   <p>{{message}}</p>
// `

})
export class AboutComponent implements AfterContentInit {
  @ContentChild(AppListIconDirective, { static: false })
  public listDirective: AppListIconDirective;


  public list = [{ name: 'Hello', icon: "My Icon"}, { name: "Another", icon: "My Icon 3"}, { name: "One", icon: "My Icon 5"}]
  // tslint:disable-next-line: variable-name
  constructor(@Inject(BOARD_DAO_TOKEN)  public message: string) { }


  ngAfterContentInit(): void {
    const item = this.listDirective;
    debugger
  }
}


